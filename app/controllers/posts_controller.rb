class PostsController < ApplicationController
    before_action :post_find, only: [:edit, :update, :show, :destroy]

    before_action :authenticate_user!, except: [:index, :show]

    def index
        @posts = Post.all
    end

    def show
    end

    def new
        @post = Post.new
        
    end

    def create
        @post = Post.new(post_params)

        respond_to do |format|
            if @post.save
              format.html { redirect_to @post, warning: 'Post Creado Exitosamente.' }
              format.json { render :show, status: :created, location: @post }
            else
              format.html { render :new }
              format.json { render json: @post.errors, status: :unprocessable_entity }
            end
        end
    end

    def update
        respond_to do |format|
            if @post.update(post_params)
              format.html { redirect_to @post, warning: 'Post Editado Exitosamente.' }
              format.json { render :show, status: :ok, location: @post }
            else
              format.html { render :edit }
              format.json { render json: @post.errors, status: :unprocessable_entity }
            end
        end
    end

    def destroy
        @post.destroy
            respond_to do |format|
                format.html { redirect_to posts_url, warning: 'Post Borrado Exitosamente.' }
                format.json { head :no_content }
        end
    end

    def post_find
        @post = Post.find(params[:id])

    end

    private
        def post_params
            params.require(:post).permit(:title, :body)
        end
    
end
